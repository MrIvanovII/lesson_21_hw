from flask import Blueprint
import requests

view = Blueprint("view", __name__)

@view.route("/")
def home():
    try:
        resp = requests.get("http://api3:5000")
        return f"2=>{resp.text}"
    except Exception as e:
        return str(e)
