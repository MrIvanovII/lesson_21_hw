import os
from typing import ValuesView
from flask import Flask

def create_app():
    app = Flask(__name__)
    flask_env = os.getenv("FLASK_ENV", "None")
    
    from application.routs import view

    app.register_blueprint(view)

    return app