from flask import Blueprint
import requests

view = Blueprint("view", __name__)

@view.route("/")
def home():
    try:
        resp = requests.get("http://api2:5000")
        return f"1=>{resp.text}"
    except Exception as e:
        return str(e)
